var gulp = require('gulp'),
    browserSync = require('browser-sync').create();

var reload  = browserSync.reload;

var paths = {
  styles: {
    src: 'src/assets/css/**/*.css',
    dest: 'public/assets/css/'
  },
  php: {
    src: 'src/**/*.php',
    dest: 'public/'
  }
};

gulp.task('browser-sync', function() {
  browserSync.init({
      proxy: '127.0.0.1:80',
      port: 8080,
      open: true,
      notify: false
  });
});

gulp.task('styles', function () {
  return gulp.src(paths.styles.src)
    .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('phps', function() {
  return gulp.src(paths.php.src)
    .pipe(gulp.dest(paths.php.dest));
});

gulp.task('watch', gulp.series('browser-sync'), function() {
  gulp.watch(paths.php.src).on('change', gulp.series('phps', reload));
  gulp.watch(paths.styles.src).on('change', gulp.series('styles', reload));
});

var build = gulp.parallel('styles', 'phps');

exports.default = build;